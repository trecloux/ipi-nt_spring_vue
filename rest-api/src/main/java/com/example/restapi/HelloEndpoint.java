package com.example.restapi;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloEndpoint {

    @GetMapping("/hello")
    public Hello hello(@RequestParam String message) {
        return new Hello("Welcome to " + message);

    }
}
