package com.example.restapi;

public class Hello {
    private final String hello;

    public Hello(String hello) {
        this.hello = hello;
    }

    public String getHello() {
        return hello;
    }
}
